#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import sys
import os
from importlib import import_module

def dynamic_import(abs_module_path, class_name):
    module_object = import_module(abs_module_path)
    target_class = getattr(module_object, class_name)
    return target_class

def getTest(testClass):
    module = dynamic_import('tests.%s' % testClass, str.capitalize(testClass))
    try:
        flags = { 'testCases': [] }
        flags['testCases'].append(module)
        return flags
    except NameError as e:
        from defs.functions import red
        print (red("\n** Error: No se ha definido la clase: %s" % testClass))
        assert False
