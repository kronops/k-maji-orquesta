#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from unittest import TestCase
from defs import functions as f

class Nagios(TestCase):
    def test_service_running(s):
        services = ['apache2', 'nagios']
        f.test_service_running(s.host, services, s.output)
        f.test_service_enabled(s.host, services, s.output)
