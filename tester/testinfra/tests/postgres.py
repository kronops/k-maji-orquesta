#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from unittest import TestCase
from defs import functions as f

class Postgres(TestCase):
    def test_pkgs(s):
        packages = ['postgresql-client-9.5',
                    'postgresql-contrib-9.5',
                    'postgresql-9.5']
        f.test_pkgs(s.host, packages, s.output)

    def test_services(s):
        f.test_service_running(s.host, ['postgresql'], s.output)
        f.test_service_enabled(s.host, ['postgresql'], s.output)


    def test_ports(s):
        ports = ['127.0.0.1:5432/tcp']
        f.test_ports(s.host, ports, s.output)
