#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from unittest import TestCase
from defs import functions as f

class System(TestCase):
    def test_pkgs(s):
        packages = ['rsyslog', 'openssh-server', 'ntp']
        f.test_pkgs(s.host, packages, s.output)

    def test_ports(s):
        ports = [':::22/tcp']
        f.test_ports(s.host, ports, s.output)

    def test_services(s):
        services = ['rsyslog','ssh','ntp']
        f.test_service_running(s.host, services, s.output)
        f.test_service_enabled(s.host, services, s.output)
