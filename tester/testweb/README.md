# Automatizando pruebas web con Python y Selenium

# Table of contents

1. [Introducción](#introducci-n)
2. [Requisitos](#requisitos)

## Introducción

En esta documento mostraremos como usar la herramienta Selenium junto con algo de código simple de Python 3 para automatizar la ejecución de pruebas de flujos web, esto permitirá integrar y mejorar la calidad de las liberaciones y despliegues de las webapps.

### Objetivos

El objetivo de automatizar las pruebas web es definir un flujo que nos permita validar:

 - El despliegue correcto de nuestra aplicación web.
 - La existencia de algún bug dentro del flujo de la aplicación.

### Requisitos

Antes de construir el proyecto antes debe definir unas variables de ambiente que definirán el nombre del projecto y el nombre del ambiente para el cual se ejecutarán las pruebas, para hacerlo, el script Makefile toma las variables desde el archivo .env, quedando algo así:

```shell
$ cat .env
PROJECT_NAME=local
PROJECT_ENV=development
export $PROJECT_NAME
export $PROJECT_ENV

```

En el nodo en donde se ejecutan las pruebas se deben instalar los módulos de python que se listan en el archivo requirements.txt.

Si usa ansible para desplegar el k-maji-orquesta, entonces es mejor usar ansible y el rol tester-service, este ya automatiza el despliegue de un servidor para automatización de pruebas de infraestructura y web.

### Pruebas

Para ejecutar las pruebas se usa el binario python3.5 y se ejecuta el script runTests.py, o también puede usar make test, por ejemplo:

```shell
$ make test

```

El script genera una salida que se envía a XMLRunner para generar un reporte en formato XML adecuado para JUnit y que pueda ser cargado en Jenkins.


### Casos de pruebas definidos

 - OpenUrl: Lanza un navegador a y carga una url dada por el usuario. Regresa la evidencia si la url pude ser cargada o no.

### Referencias

En esta sección hacemos referencia a documentación que usamos de ejemplo para construir las pruebas y su integración con otras herramientas de Orquestación de TI.

Selenium code:
https://www.seleniumhq.org/
