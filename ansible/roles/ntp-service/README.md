ntp-service
===========

Tasks for setting up local ntp service.

Requirements
------------

Ubuntu based system.

Role Variables
--------------

Default ntp servers:

ntp_timeserver1: swisstime.ee.ethz.ch
ntp_timeserver2: mx.pool.ntp.org

Dependencies
------------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - ntp-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@kronops.com.mx.
