---
# tasks file for wordpress-service

- name: Install PHP runtime for Wordpress
  apt:
    name:
      - php7.4
      - php7.4-common
      - php7.4-cli
      - php7.4-gd
      - php7.4-curl
      - php7.4-json
      - php7.4-readline
      - php7.4-mysql
      - php7.4-dev
      - php7.4-redis
      - php-pear
    state: present
  tags:
    - wordpress_service

- name: Install wordpress virtualhost config for apache
  template: src=virtualhost.conf.j2
    dest=/etc/apache2/sites-available/{{ dns_public_domain }}.conf
    owner=root
    group=root
    mode=0644
  tags:
    - wordpress_service

- name: Create wordpress apache root directory
  file: path=/var/www/{{ dns_public_domain }} state=directory
    owner=root
    group=root
    mode=0755
  tags:
    - wordpress_service

- name: Create wordpress apache htdocs directory
  file: path=/var/www/{{ dns_public_domain }}/htdocs state=directory
    owner=root
    group=root
    mode=0755
  tags:
    - wordpress_service

- name: Create wordpress apache logs directory
  file: path=/var/www/{{ dns_public_domain }}/logs state=directory
    owner=root
    group=root
    mode=0755
  tags:
    - wordpress_service

- name: Enable woredpress virtualhost
  command: a2ensite {{ dns_public_domain }}
  notify:
    - restart apache2
  tags:
    - wordpress_service

- name: Install template for php-apache2 wordpress settings
  template: src=php-wordpress.ini.j2
    dest=/etc/php/7.4/mods-available/wordpress.ini
    owner=root
    group=root
    mode=0644
  tags:
    - wordpress_service

- name: Enable php apache wordpress settings
  command: phpenmod wordpress
  notify:
    - restart apache2
  tags:
    - wordpress_service

- name: Delete wordpress database on mysql
  mysql_db: name={{ wp_mysql_db }} state=absent
  tags:
    - wordpress_service

- name: Create wordpress database on mysql
  mysql_db: name={{ wp_mysql_db }} state=present
  tags:
    - wordpress_service

- name: Create wordpress database user on mysql
  mysql_user:
    name={{ wp_mysql_user }}
    password={{ wp_mysql_password }}
    priv=*.*:ALL
  ignore_errors: yes
  tags:
    - wordpress_service

- name: Download WordPress from latest tarball
  get_url: url=https://wordpress.org/latest.tar.gz
    dest=/tmp/wordpress.tar.gz
    validate_certs=no
  tags:
    - wordpress_service_app_deploy
    - wordpress_service

- name: Extract WordPress
  shell: tar zxvf /tmp/wordpress.tar.gz -C /tmp/
  tags:
    - wordpress_service_app_deploy
    - wordpress_service

- name: Copy wordpress data files
  command: rsync -a /tmp/wordpress/ /var/www/{{ dns_public_domain }}/htdocs/
  tags:
    - wordpress_service_app_deploy
    - wordpress_service

- name: Fetch random salts for WordPress config
  local_action: command curl https://api.wordpress.org/secret-key/1.1/salt/
  register: "wp_salt"
  tags:
    - wordpress_service_app_deploy
    - wordpress_service

- name: Install wp-config.php file
  template: src=wp-config.php.j2
    dest=/var/www/{{ dns_public_domain }}/htdocs/wp-config.php
    owner=root
    group=www-data
    mode=0640
  tags:
    - wordpress_service_app_deploy
    - wordpress_service

- name: Set ownership wordpress directory
  file:
    path: "/var/www/{{ dns_public_domain }}"
    state: directory
    recurse: yes
    owner: www-data
    group: www-data
  tags:
    - wordpress_service

- name: Set permissions for application directories
  shell: "/usr/bin/find /var/www/{{ dns_public_domain }}/ -type d -exec chmod 750 {} \\;"
  tags:
    - wordpress_service

- name: Set permissions for application files
  shell: "/usr/bin/find /var/www/{{ dns_public_domain }}/ -type f -exec chmod 640 {} \\;"
  tags:
    - wordpress_service

- name: Download wordpress cli
  shell: curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
  tags:
    - wordpress_service

- name: Move WP-CLI to /usr/local/bin/wp
  command: mv /root/wp-cli.phar /usr/local/bin/wp
  tags:
    - wordpress_service

- name: Change wp cli permissions
  file:
    path: /usr/local/bin/wp
    mode: u=rwx,g=rx,o=rx
  tags:
    - wordpress_service

- name: Create system group for web development
  group:
    name: '{{ wordpress_webdev_group }}'
    state: present
  tags:
    - wordpress_service

- name: Create system user for web development
  user: name={{ wordpress_webdev_username }}
    shell=/bin/bash
    groups={{ wordpress_webdev_group }}
    update_password=always
    password={{ wordpress_webdev_userpass_hash }}
  tags:
    - wordpress_service

- name: Change permisions on web root directory
  command: echo "Change permisions"
  tags:
    - wordpress_service
