Role Name
==========

The purpose of this role...

Requirements
------------

Ubuntu based system.

Role Variables
--------------

Dependencies
------------

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - role-template

License
-------

MIT

Author Information
------------------

Please any question, please contact the author at: member@kronops.com.mx.
