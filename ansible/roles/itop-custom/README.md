itop-custom
============

Tasks for setting up itop theme.

Requirements
------------

Ubuntu based system.

Role Variables
--------------

# These are the iTop database settings

# Which version of iTop to deploy
itop_version: 2.2.0-2459
itop_sha256sum: 42ca594afc709cbef8528a6096f5a1efe96dcf3164e7ce321e87d57ae015cc82

Dependencies
------------

This role depends on the follow roles:

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
        - itop-custom

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: luis.carrillo@kronops.com.mx.
