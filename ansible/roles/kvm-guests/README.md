kvm-guests
==========

Tasks for setting system tunning for kvm guest virtual machines.

Requirements
------------

Ubuntu based system running on KVM/QEMU virtual machine.

Role Variables
--------------

Not parameters.

Role Tags
---------

 - kvm_guests

Dependencies
------------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - kvm-guests

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@kronops.com.mx.
