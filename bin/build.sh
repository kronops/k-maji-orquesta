#!/bin/bash

#
# script: build.sh
# author: jorge.medina@kronops.com.mx
# desc: Script to build the deployment environment based on ansible

#
# Requerements:
# available projects: local
# available environments: development, testing, production.

# Enable debug mode and log to file
export DEBUG=1
LOG=/var/log/build.log
[ -n "$DEBUG" ] && exec < /dev/stdin > $LOG 2>&1

# Bash debug mode
[ -n "$DEBUG" ] && set -x

# Stop on errors
set -e

# vars
source .env

# Functions

# Log function
log() {
    echo -n `date`
    echo -n " -- "
    echo $@
}

# Abort function
abort() {
  echo "$@"
  log "$@"
  exit 1
}

# Build function
build() {
  # Replacing custom variables
  # We change the path at build time from vagrant/jenkins.
  sed -i \
    -e "s^%PROJECT%^$PROJECT_NAME^g" \
    -e "s^%ENVIRONMENT%^$PROJECT_ENV^g" \
    ansible/ansible.cfg || abort "Error while replacing with sed s/%VARS%/\$VARS/g on ansible.cfg and inventory/$PROJECT/$ENVIRONMENT."

  [[ "$BRANCH" == 'production' ]] && WARNINGS=False || WARNINGS=True
  sed "s^%WARNINGS%^$WARNINGS^g" -i ansible/ansible.cfg

}
# main
# Pre Build process
build
