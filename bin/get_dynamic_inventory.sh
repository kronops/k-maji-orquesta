#!/bin/bash

set -e

GROUP=$1
ENV_FILE=/var/lib/jenkins/.env
SCRIPT=FromITOPtoAnsible.sh
[[ -f "ENV_FILE" ]] && { echo "[Error]: file $ENV_FILE not found!!"; exit 1; }
[[ -z "$GROUP" ]] && { echo "usage: ./$0 SERVER-GROUP"; exit 1; }
source $ENV_FILE
KEY=inventory/${PROJECT_NAME}/.ssh/id-${PROJECT_NAME}-ansible.rsa
export OQL="SELECT VirtualMachine WHERE virtualhost_name = '$GROUP'"
cd /etc/ansible
ansible all -i $SCRIPT -m shell -m "ping" -e "ansible_ssh_private_key_file=$KEY"

exit 0
